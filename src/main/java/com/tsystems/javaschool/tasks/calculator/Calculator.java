package com.tsystems.javaschool.tasks.calculator;

import com.sun.deploy.util.StringUtils;

public class Calculator {
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if(statement == "" || statement == null || statement.contains(",")){
            return null;
        }
        int openBracket = statement.indexOf('(');
        if(openBracket!=-1){
            int closeBracket = -1;
            int openBracketsCount = 1;
            int closeBracketsCount = 0;
            for (int i = openBracket+1; i < statement.length(); i++) {
                if(statement.charAt(i) == '('){
                    openBracketsCount++;
                }
                if(statement.charAt(i) == ')'){
                    closeBracketsCount++;
                    if(openBracketsCount == closeBracketsCount){
                        closeBracket = i;
                        break;
                    }
                }
            }
            if(closeBracket == -1){
                return null;
            }
            else {
                if(openBracket == 0 && closeBracket == statement.length() - 1){
                    return evaluate(statement.substring(1, statement.length() - 1));
                }
                else if((closeBracket == statement.length() - 2) ||(openBracket == 1)){
                    return null;
                }
                else if(openBracket == 0 && closeBracket != statement.length() - 1){
                    String evaluation = evaluate(statement.substring(1, closeBracket));
                    return evaluate(evaluation + statement.substring(closeBracket + 1));
                }
                else if(openBracket!=0 && closeBracket == statement.length() -1){
                    String evaluation = evaluate(statement.substring(openBracket+1, closeBracket));
                    return evaluate(statement.substring(0, openBracket) + evaluation);
                }
                else{
                    String evaluation = evaluate(statement.substring(openBracket + 1, closeBracket));
                    return evaluate(statement.substring(0, openBracket) + evaluation + statement.substring(closeBracket + 1));
                }
            }
        }
        int multiplication = statement.indexOf('*');
        int division = statement.indexOf('/');
        String firstNumber = "";
        String secondNumber = "";


        //multiplication
        if((multiplication < division || division == -1)&& multiplication!= -1 ){
            int countDotsFirst = 0;
            int start = -1;
            for (int i = multiplication-1; i >= 0; i--) {
                if(statement.charAt(i) == '/' || statement.charAt(i) == '*' || statement.charAt(i) == '+' || (statement.charAt(i) == '-' && i!=0)){
                    if(i == multiplication -1 ){
                        return null;
                    }
                    else{
                        start = i + 1;
                        break;
                    }
                }
                if(statement.charAt(i) == '.'){
                    countDotsFirst++;
                    if(countDotsFirst > 1){
                        return null;
                    }
                }

                firstNumber = statement.charAt(i) + firstNumber;


            }
            int countDotsSecond = 0;
            int stop = -1;
            for (int i = multiplication + 1; i < statement.length(); i++) {
                if(statement.charAt(i) == '/' || statement.charAt(i) == '*' || statement.charAt(i) == '+' || (statement.charAt(i) == '-' && i!=multiplication+1)){
                    if(i == multiplication +1 ){
                        return null;
                    }
                    else{
                        stop = i - 1;
                        break;
                    }
                }
                else {
                    secondNumber = secondNumber + statement.charAt(i);
                }
                if(statement.charAt(i) == '.'){
                    countDotsSecond++;
                    if(countDotsSecond > 1){
                        return null;
                    }
                }
            }
            if((secondNumber == "" )|| (firstNumber == "")){
                return null;
            }
            if(countDotsFirst == 1){
                double firstDoubleNumber = Double.parseDouble(firstNumber);
                if(countDotsSecond == 1){
                    double secondDoubleNumber = Double.parseDouble(secondNumber);
                    double result = firstDoubleNumber*secondDoubleNumber;


                    if(start == -1 && stop == -1){
                        result = result * 10000;
                        int i = (int) Math.round(result);
                        result = (double)i / 10000;
                        return String.valueOf(result);
                    }
                    else if(start == -1 && stop != -1){
                        return evaluate(String.valueOf(result) + statement.substring(stop+1));
                    }
                    else if(start!=-1 && stop == -1){
                        return evaluate(statement.substring(0, start) + String.valueOf(result));
                    }
                    else{
                        return evaluate(statement.substring(0, start) + String.valueOf(result)+ statement.substring(stop+1));
                    }
                }
                else {
                    int secondIntNumber = Integer.parseInt(secondNumber);
                    double result = firstDoubleNumber*secondIntNumber;


                    if(start == -1 && stop == -1){
                        result = result * 10000;
                        int i = (int) Math.round(result);
                        result = (double)i / 10000;
                        return String.valueOf(result);
                    }
                    else if(start == -1 && stop != -1){
                        return evaluate(String.valueOf(result) + statement.substring(stop+1));
                    }
                    else if(start!=-1 && stop == -1){
                        return evaluate(statement.substring(0, start) + String.valueOf(result));
                    }
                    else{
                        return evaluate(statement.substring(0, start) + String.valueOf(result)+ statement.substring(stop+1));
                    }
                }

            }
            else{
                int firstIntNumber = Integer.parseInt(firstNumber);
                if(countDotsSecond == 1){
                    double secondDoubleNumber = Double.parseDouble(secondNumber);
                    double result = firstIntNumber*secondDoubleNumber;

                    if(start == -1 && stop == -1){
                        result = result * 10000;
                        int i = (int) Math.round(result);
                        result = (double)i / 10000; 
                        return String.valueOf(result);
                    }
                    else if(start == -1 && stop != -1){
                        return evaluate(String.valueOf(result) + statement.substring(stop+1));
                    }
                    else if(start!=-1 && stop == -1){
                        return evaluate(statement.substring(0, start) + String.valueOf(result));
                    }
                    else{
                        return evaluate(statement.substring(0, start) + String.valueOf(result)+ statement.substring(stop+1));
                    }
                }
                else {
                    int secondIntNumber = Integer.parseInt(secondNumber);
                    int result = firstIntNumber*secondIntNumber;
                    if(start == -1 && stop == -1){
                        return String.valueOf(result);
                    }
                    else if(start == -1 && stop != -1){
                        return evaluate(String.valueOf(result) + statement.substring(stop+1));
                    }
                    else if(start!=-1 && stop == -1){
                        return evaluate(statement.substring(0, start) + String.valueOf(result));
                    }
                    else{
                        return evaluate(statement.substring(0, start) + String.valueOf(result)+ statement.substring(stop+1));
                    }
                }
            }
        }




        //division
        else if(division!=-1){
            int countDotsFirst = 0;
            int start = -1;
            for (int i = division-1; i >= 0; i--) {
                if(statement.charAt(i) == '/' || statement.charAt(i) == '*' || statement.charAt(i) == '+' || (statement.charAt(i) == '-' && i!=0)){
                    if(i == division -1 ){
                        return null;
                    }
                    else{
                        start = i + 1;
                        break;
                    }
                }
                if(statement.charAt(i) == '.'){
                    countDotsFirst++;
                    if(countDotsFirst > 1){
                        return null;
                    }
                }
                else{
                    firstNumber = statement.charAt(i) + firstNumber;
                }
            }
            int countDotsSecond = 0;
            int stop = -1;
            for (int i = division + 1; i < statement.length(); i++) {
                if(statement.charAt(i) == '/' || statement.charAt(i) == '*' || statement.charAt(i) == '+' || (statement.charAt(i) == '-' && i!= division+1)){
                    if(i == division +1 ){
                        return null;
                    }
                    else{
                        stop = i - 1;
                        break;
                    }
                }
                else {
                    secondNumber = secondNumber + statement.charAt(i);
                }
                if(statement.charAt(i) == '.'){
                    countDotsSecond++;
                    if(countDotsSecond > 1){
                        return null;
                    }
                }
            }
            if((secondNumber == "" )|| (firstNumber == "")){
                return null;
            }
            if(Integer.parseInt(secondNumber) == 0){
                return null;
            }
            if(countDotsFirst == 1){
                double firstDoubleNumber = Double.parseDouble(firstNumber);
                if(countDotsSecond == 1){
                    double secondDoubleNumber = Double.parseDouble(secondNumber);
                    double result = firstDoubleNumber/secondDoubleNumber;


                    if(start == -1 && stop == -1){
                        result = result * 10000;
                        int i = (int) Math.round(result);
                        result = (double)i / 10000;
                        return String.valueOf(result);
                    }
                    else if(start == -1 && stop != -1){
                        return evaluate(String.valueOf(result) + statement.substring(stop+1));
                    }
                    else if(start!=-1 && stop == -1){
                        return evaluate(statement.substring(0, start) + String.valueOf(result));
                    }
                    else{
                        return evaluate(statement.substring(0, start) + String.valueOf(result)+ statement.substring(stop+1));
                    }
                }
                else {
                    int secondIntNumber = Integer.parseInt(secondNumber);
                    double result = firstDoubleNumber/secondIntNumber;

                    if(start == -1 && stop == -1){
                        result = result * 10000;
                        int i = (int) Math.round(result);
                        result = (double)i / 10000;
                        return String.valueOf(result);
                    }
                    else if(start == -1 && stop != -1){
                        return evaluate(String.valueOf(result) + statement.substring(stop+1));
                    }
                    else if(start!=-1 && stop == -1){
                        return evaluate(statement.substring(0, start) + String.valueOf(result));
                    }
                    else{
                        return evaluate(statement.substring(0, start) + String.valueOf(result)+ statement.substring(stop+1));
                    }
                }

            }
            else{
                int firstIntNumber = Integer.parseInt(firstNumber);
                if(countDotsSecond == 1){
                    double secondDoubleNumber = Double.parseDouble(secondNumber);
                    double result = firstIntNumber/secondDoubleNumber;

                    if(start == -1 && stop == -1){
                        result = result * 10000;
                        int i = (int) Math.round(result);
                        result = (double)i / 10000;
                        return String.valueOf(result);
                    }
                    else if(start == -1 && stop != -1){
                        return evaluate(String.valueOf(result) + statement.substring(stop+1));
                    }
                    else if(start!=-1 && stop == -1){
                        return evaluate(statement.substring(0, start) + String.valueOf(result));
                    }
                    else{
                        return evaluate(statement.substring(0, start) + String.valueOf(result)+ statement.substring(stop+1));
                    }
                }
                else {
                    int secondIntNumber = Integer.parseInt(secondNumber);
                    double result =(double) firstIntNumber/secondIntNumber;
                    if(Math.ceil(result) == Math.floor(result)){
                       int resultInt = (int)result;
                        if(start == -1 && stop == -1){
                            return String.valueOf(resultInt);
                        }
                        else if(start == -1 && stop != -1){
                            return evaluate(String.valueOf(resultInt) + statement.substring(stop+1));
                        }
                        else if(start!=-1 && stop == -1){
                            return evaluate(statement.substring(0, start) + String.valueOf(resultInt));
                        }
                        else{
                            return evaluate(statement.substring(0, start) + String.valueOf(resultInt)+ statement.substring(stop+1));
                        }
                    }
                    else if(start == -1 && stop == -1) {
                        result = result * 10000;
                        int i = (int) Math.round(result);
                        result = (double)i / 10000;
                        return String.valueOf(result);
                    }
                    else if(start == -1 && stop != -1){
                        return evaluate(String.valueOf(result) + statement.substring(stop+1));
                    }
                    else if(start!=-1 && stop == -1){
                        return evaluate(statement.substring(0, start) + String.valueOf(result));
                    }
                    else{
                        return evaluate(statement.substring(0, start) + String.valueOf(result)+ statement.substring(stop+1));
                    }

                }
            }
        }





        int sum = statement.indexOf('+');
        int difference = statement.indexOf('-');
        firstNumber = "";
        secondNumber = "";


        //difference
            if((difference < sum || sum == -1) && (difference!= -1) && statement.charAt(0) != '-'){

                int countDotsFirst = 0;
                int start = -1;
                for (int i = difference-1; i >= 0; i--) {
                    if(statement.charAt(i) == '/' || statement.charAt(i) == '*' || statement.charAt(i) == '+' || statement.charAt(i) == '-'){
                        if(i == difference -1 ){
                            return null;
                        }
                        else{
                            start = i + 1;
                            break;
                        }
                    }
                    if(statement.charAt(i) == '.'){
                        countDotsFirst++;
                        if(countDotsFirst > 1){
                            return null;
                        }
                    }
                    else{
                        firstNumber = statement.charAt(i) + firstNumber;
                    }
                }
                int countDotsSecond = 0;
                int stop = -1;
                for (int i = difference + 1; i < statement.length(); i++) {
                    if(statement.charAt(i) == '/' || statement.charAt(i) == '*' || statement.charAt(i) == '+' || statement.charAt(i) == '-'){
                        if(i ==difference +1 ){
                            return null;
                        }
                        else{
                            stop = i - 1;
                            break;
                        }
                    }
                    else {
                        secondNumber = secondNumber + statement.charAt(i);
                    }
                    if(statement.charAt(i) == '.'){
                        countDotsSecond++;
                        if(countDotsSecond > 1){
                            return null;
                        }
                    }
                }
                if((secondNumber == "" )){
                    return null;
                }
                if (firstNumber == "") {

                }
                if(countDotsFirst == 1){
                    double firstDoubleNumber = Double.parseDouble(firstNumber);
                    if(countDotsSecond == 1){
                        double secondDoubleNumber = Double.parseDouble(secondNumber);
                        double result = firstDoubleNumber-secondDoubleNumber;
                        if(start == -1 && stop == -1){
                            return String.valueOf(result);
                        }
                        else if(start == -1 && stop != -1){
                            evaluate(String.valueOf(result) + statement.substring(stop+1));
                        }
                        else if(start!=-1 && stop == -1){
                            evaluate(statement.substring(0, start) + String.valueOf(result));
                        }
                        else{
                            evaluate(statement.substring(0, start) + String.valueOf(result)+ statement.substring(stop+1));
                        }
                    }
                    else {
                        int secondIntNumber = Integer.parseInt(secondNumber);
                        double result = firstDoubleNumber-secondIntNumber;
                        if(start == -1 && stop == -1){
                            return String.valueOf(result);
                        }
                        else if(start == -1 && stop != -1){
                            evaluate(String.valueOf(result) + statement.substring(stop+1));
                        }
                        else if(start!=-1 && stop == -1){
                            evaluate(statement.substring(0, start) + String.valueOf(result));
                        }
                        else{
                            evaluate(statement.substring(0, start) + String.valueOf(result)+ statement.substring(stop+1));
                        }
                    }

                }
                else{
                    int firstIntNumber = Integer.parseInt(firstNumber);
                    if(countDotsSecond == 1){
                        double secondDoubleNumber = Double.parseDouble(secondNumber);
                        double result = firstIntNumber-secondDoubleNumber;
                        if(start == -1 && stop == -1){
                            return String.valueOf(result);
                        }
                        else if(start == -1 && stop != -1){
                            evaluate(String.valueOf(result) + statement.substring(stop+1));
                        }
                        else if(start!=-1 && stop == -1){
                            evaluate(statement.substring(0, start) + String.valueOf(result));
                        }
                        else{
                            evaluate(statement.substring(0, start) + String.valueOf(result)+ statement.substring(stop+1));
                        }
                    }
                    else {
                        int secondIntNumber = Integer.parseInt(secondNumber);
                        int result = firstIntNumber-secondIntNumber;
                        if(start == -1 && stop == -1){
                            return String.valueOf(result);
                        }
                        else if(start == -1 && stop != -1){
                            return evaluate(String.valueOf(result) + statement.substring(stop+1));
                        }
                        else if(start!=-1 && stop == -1){
                            return evaluate(statement.substring(0, start) + String.valueOf(result));
                        }
                        else{
                            return evaluate(statement.substring(0, start) + String.valueOf(result)+ statement.substring(stop+1));
                        }
                    }
                }
            }





        //sum
        if(sum!=-1){
            int countDotsFirst = 0;
            int start = -1;
            firstNumber = "";
            for (int i = sum-1; i >= 0; i--) {
                if(statement.charAt(i) == '/' || statement.charAt(i) == '*' || statement.charAt(i) == '+'){
                    if(i == sum - 1 ){
                        return null;
                    }
                    else{
                        start = i + 1;
                        break;
                    }
                }
                if(statement.charAt(i) == '.'){
                    countDotsFirst++;
                    if(countDotsFirst > 1){
                        return null;
                    }
                }
                else{
                    firstNumber = statement.charAt(i) + firstNumber;
                }
            }
            int countDotsSecond = 0;
            int stop = -1;
            secondNumber = "";
            for (int i = sum + 1; i < statement.length(); i++) {
                if(statement.charAt(i) == '/' || statement.charAt(i) == '*' || statement.charAt(i) == '+' || statement.charAt(i) == '-'){
                    if(i == sum +1 ){
                        return null;
                    }
                    else{
                        stop = i - 1;
                        break;
                    }
                }
                else {
                    secondNumber = secondNumber + statement.charAt(i);
                }
                if(statement.charAt(i) == '.'){
                    countDotsSecond++;
                    if(countDotsSecond > 1){
                        return null;
                    }
                }
            }
            if((secondNumber == "" )|| (firstNumber == "")){
                return null;
            }
            if(countDotsFirst == 1){
                double firstDoubleNumber = Double.parseDouble(firstNumber);
                if(countDotsSecond == 1){
                    double secondDoubleNumber = Double.parseDouble(secondNumber);
                    double result = firstDoubleNumber + secondDoubleNumber;
                    if(start == -1 && stop == -1){
                        return String.valueOf(result);
                    }
                    else if(start == -1 && stop != -1){
                        evaluate(String.valueOf(result) + statement.substring(stop+1));
                    }
                    else if(start!=-1 && stop == -1){
                        evaluate(statement.substring(0, start) + String.valueOf(result));
                    }
                    else{
                        evaluate(statement.substring(0, start) + String.valueOf(result)+ statement.substring(stop+1));
                    }
                }
                else {
                    int secondIntNumber = Integer.parseInt(secondNumber);
                    double result = firstDoubleNumber+secondIntNumber;
                    if(start == -1 && stop == -1){
                        return String.valueOf(result);
                    }
                    else if(start == -1 && stop != -1){
                        evaluate(String.valueOf(result) + statement.substring(stop+1));
                    }
                    else if(start!=-1 && stop == -1){
                        evaluate(statement.substring(0, start) + String.valueOf(result));
                    }
                    else{
                        evaluate(statement.substring(0, start) + String.valueOf(result)+ statement.substring(stop+1));
                    }
                }

            }
            else{
                int firstIntNumber = Integer.parseInt(firstNumber);
                if(countDotsSecond == 1){
                    double secondDoubleNumber = Double.parseDouble(secondNumber);
                    double result = firstIntNumber+secondDoubleNumber;

                    if(start == -1 && stop == -1){
                        result = result * 10000;
                        int i = (int) Math.round(result);
                        result = (double)i / 10000;
                        return String.valueOf(result);
                    }
                    else if(start == -1 && stop != -1){
                        evaluate(String.valueOf(result) + statement.substring(stop+1));
                    }
                    else if(start!=-1 && stop == -1){
                        evaluate(statement.substring(0, start) + String.valueOf(result));
                    }
                    else{
                        evaluate(statement.substring(0, start) + String.valueOf(result)+ statement.substring(stop+1));
                    }
                }
                else {
                    int secondIntNumber = Integer.parseInt(secondNumber);
                    int result = firstIntNumber+secondIntNumber;
                    if(start == -1 && stop == -1){
                        return String.valueOf(result);
                    }
                    else if(start == -1 && stop != -1){
                        evaluate(String.valueOf(result) + statement.substring(stop+1));
                    }
                    else if(start!=-1 && stop == -1){
                        evaluate(statement.substring(0, start) + String.valueOf(result));
                    }
                    else{
                        evaluate(statement.substring(0, start) + String.valueOf(result)+ statement.substring(stop+1));
                    }
                }
            }
        }
        return "";
    }

}
