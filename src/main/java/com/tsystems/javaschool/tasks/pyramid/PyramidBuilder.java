package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {
    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) { 
        // TODO : Implement your solution here
        if(inputNumbers.contains(null)){
            throw new CannotBuildPyramidException();
        }
        int length = inputNumbers.size();
        int i = 1;
        while (true){
            length-=i;
            if(length == 0){
                break;
            }
            else if(length < 0){
                throw new CannotBuildPyramidException();
            }
            i++;
        }
        Collections.sort(inputNumbers);
        int numbers = 0;
        int pyramid[][] = new int[i][2*i-1];
        for (int j = 0; j < i; j++) {
            int numberOfZeros = 2*i-1-(j+1) - j;
            if(numberOfZeros!= 0) {
                for (int k = numberOfZeros/2; k < numberOfZeros/2+ 2*(j+1) - 1; k++) {
                    if ((k-numberOfZeros/2) % 2 == 0) {
                        pyramid[j][k] = inputNumbers.get(numbers);
                        numbers++;
                    } else {
                        pyramid[j][k] = 0;
                    }
                }
            }
            else {
                for (int k = 0; k < 2 * (j+1) - 1; k++) {
                    if (k % 2 == 0) {
                        pyramid[j][k] = inputNumbers.get(numbers);
                        numbers++;
                    } else {
                        pyramid[j][k] = 0;
                    }
                }
            }



        }
        return pyramid;
    }


}
